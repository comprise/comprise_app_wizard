import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WebviewDirective } from '../directives/webview.directive';
import { CpRoutingModule } from './cp-routing.module';
import { CpComponent } from './cp.component';


@NgModule({
  declarations: [CpComponent, WebviewDirective],
  imports: [
    CommonModule,
    CpRoutingModule
  ]
})
export class CpModule { }
