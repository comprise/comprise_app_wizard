import { Component, AfterViewInit, OnDestroy  } from '@angular/core';
import { Router } from '@angular/router';
import { remote, shell } from 'electron';
import { ProjectDataService, ElectronService, RestService, LoggingService } from '../core/services';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
const BrowserView = remote.BrowserView;  
import * as path from 'path';

@Component({
  selector: 'app-cp',
  templateUrl: './cp.component.html',
  styleUrls: ['./cp.component.scss']
})
export class CpComponent implements AfterViewInit,OnDestroy   {

	browserView;

  	constructor(private router: Router, 
		public projectDataService: ProjectDataService,
		public restService: RestService,
		public loggingService: LoggingService,
		public electronService: ElectronService,
		public httpClient: HttpClient) {
		console.log(this.router.url)
	}
	
	resizeBrowserView() {
		this.browserView.setBounds({ 
			x: Math.ceil(window.innerWidth * 0.1497), 
			y: Math.ceil(window.innerHeight * 0.049), 
			width: Math.floor(window.innerWidth * 0.8503), 
			height: Math.floor(window.innerHeight * 0.649)
		})
	}
	
	ngOnDestroy() { 
		this.electronService.electronWindow.setBrowserView(null)
		this.loggingService.showLog();
	}	
	
	getConfiguration(issuer) {
        return fetch(issuer + "/.well-known/openid-configuration").then(response => response.ok ? response.json() : Promise.reject(response));
	}


	async ngAfterViewInit() { 
		let env = this; 
		this.browserView = new BrowserView({
			webPreferences: {
				
			}
		});
		if(env.projectDataService.projectName !== "none") {
			this.electronService.electronWindow.setBrowserView(this.browserView)
			
			var issuer = "https://comprisedevb2c.b2clogin.com/comprisedevb2c.onmicrosoft.com/b2c_1_default/v2.0/";        
			// use Cloud Platform Web UI credentials (not sure if we need to create separate)
			var client_id = "686fc6d3-389e-4580-bc84-d19007cebc5d";
			var client_secret = "b7HP2VX6wv-G5DBpvDL/=Av@vP=?OQkr"; 
			var redirect_uri = "https://comprise-dev.tilde.com/dashboard";
			var scope = "openid https://comprisedevb2c.onmicrosoft.com/comprise/general"

			var config = await this.getConfiguration(issuer)
		 
			var authorization_request = config.authorization_endpoint;
			authorization_request += "?response_type=code";
			authorization_request += "&scope=" + encodeURIComponent(scope);
			authorization_request += "&client_id=" + encodeURIComponent(client_id);
			authorization_request += "&redirect_uri=" + encodeURIComponent(redirect_uri);    
			//authorization_request += "&state=VGh1IEphbiAxNiAyMDIwIDExOjU4OjU1IEdNVCswMTAwIChDZW50cmFsIEV1cm9wZWFuIFN0YW5kYXJkIFRpbWUp";
			console.log(authorization_request)
	 
			this.browserView.webContents.loadURL(authorization_request)			
			setTimeout(() => {
				env.resizeBrowserView()
			}, 500);
			
			this.electronService.electronWindow.on('resize', (e) => {
				setTimeout(() => {
					env.resizeBrowserView()
				}, 500);
			});  
			
			 
			this.browserView.webContents.on('will-redirect', function (event, newUrl) {
				setTimeout(() => {
					//console.log(event, event.sender.history)
					var code = event.sender.history[event.sender.history.length-1];

					code = code.substring(code.indexOf("code=")+5,code.length);
					event.preventDefault()
					
					let headers = new HttpHeaders();
					//headers = headers.set("Authorization", "Basic "+Buffer.from(client_id+":"+client_secret).toString('base64'));
					headers = headers.set("Content-Type", "application/x-www-form-urlencoded");			
					
					console.log( encodeURIComponent(client_secret), client_secret)
					 
					let body = new HttpParams();
					body = new HttpParams()
						.set('grant_type', encodeURIComponent('authorization_code'))
						.set('code', encodeURIComponent(code))
						.set('client_id', client_id)
						.set('client_secret', client_secret);
					
					env.httpClient.post(config.token_endpoint, body, {headers: headers}).subscribe((data: any)=>{
						console.log("GOT TOKEN", data)
						env.loggingService.info("Access to the COMPRISE Cloud Platform granted.");
						headers = new HttpHeaders();
						headers = headers.set("Authorization", "Bearer "+data.access_token);
						
						env.httpClient.post("https://comprise-dev.tilde.com/v1alpha/applications", {
							"description": env.projectDataService.projectDescription,
							"language": env.projectDataService.cloudPlatfromLanguage,
							"name": env.projectDataService.projectName
						}, {headers: headers}).subscribe((data: any)=>{
							env.loggingService.info("Application with name "+env.projectDataService.projectName+" created. Configuration data saved to 'cp.json'-file.");
							console.log("Application created", data);
							env.electronService.fs.writeFileSync(env.projectDataService.projectPath+"/cp.json",JSON.stringify(data))
							
							headers = new HttpHeaders();
							headers = headers.set("x-ms-blob-type", "BlockBlob");
							/*
							env.httpClient.get("https://comprise-dev.tilde.com/v1alpha/applications/"+data._id+"?api_key="+data.app_key,
								{headers: headers}).subscribe((auth_data: any)=>{
								console.log("auth_data", auth_data)
								env.httpClient.put(auth_data.text_upload_url,  "Text1", {headers: headers}).subscribe((res: any)=>{
									console.log("res", res)
								});	
							});
							setTimeout(() => {
								env.httpClient.get("https://comprise-dev.tilde.com/v1alpha/applications/"+data._id+"?api_key="+data.app_key,
									{headers: headers}).subscribe((auth_data: any)=>{
									console.log("auth_data2", auth_data)
									env.httpClient.put(auth_data.text_upload_url,  "Text2", {headers: headers});	
								});
							}, 3000);*/
							
							setTimeout(() => {
								env.httpClient.get("https://comprise-dev.tilde.com/v1alpha/applications/"+data._id+"?api_key="+data.app_key,
									{headers: headers}).subscribe((auth_data: any)=>{
									var audiobuffer = env.electronService.fs.readFileSync('C:/Crossplatform-Projects/Electron/comprise_app_wizard/src/assets/test.wav');
									var audioblob = new window.Blob([new Uint8Array(audiobuffer)]);
									env.httpClient.put(data.speech_upload_url, audioblob, {headers: headers}).subscribe((res)=>{
										console.log("res", res) 
									});	
								});
							}, 6000);
						});
					})  
					
					console.log("authorization_request", authorization_request)
					
					env.browserView.webContents.loadURL(newUrl);
					// More complex code to handle tokens goes here 
				
				},1000);

			});
		} 
	}
}