import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CpComponent } from './cp.component';

const routes: Routes = [
	{
		path: '',
		component: CpComponent
	}
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CpRoutingModule { }
