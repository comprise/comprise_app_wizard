import { Component, AfterViewInit, OnDestroy  } from '@angular/core';
import { Router } from '@angular/router';
import { remote } from 'electron';
import { ProjectDataService, ElectronService, RestService, LoggingService } from '../core/services';
const BrowserView = remote.BrowserView; 

@Component({
  selector: 'app-slu',
  templateUrl: './slu.component.html',
  styleUrls: ['./slu.component.scss']
})
export class SluComponent implements AfterViewInit,OnDestroy   {

	browserView;

  	constructor(private router: Router, 
		public projectDataService: ProjectDataService,
		public restService: RestService,
		public loggingService: LoggingService,
		public electronService: ElectronService) {
		console.log(this.router.url)
	}
	
	resizeBrowserView() {
		this.browserView.setBounds({ 
			x: Math.ceil(window.innerWidth * 0.15), 
			y: Math.ceil(window.innerHeight * 0.05), 
			width: Math.floor(window.innerWidth * 0.85), 
			height: Math.floor(window.innerHeight * 0.95)
		})
	}
	
	ngOnDestroy() { 
		this.browserView.destroy();
		this.loggingService.showLog();
	}	

	ngAfterViewInit(): void { 
		let env = this; 
		this.browserView = new BrowserView();
		this.electronService.electronWindow.setBrowserView(this.browserView)
		this.browserView.webContents.loadURL('https://botdashboard.tilde.ai/Account/Login/?ReturnUrl=%2F') 
		this.resizeBrowserView()
		this.electronService.electronWindow.on('resize', (e) => {
			setTimeout(() => {
				env.resizeBrowserView()
			}, 500);
		}); 
		
		this.loggingService.hideLog();
	}
}
