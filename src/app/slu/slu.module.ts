import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SluRoutingModule } from './slu-routing.module';
import { SluComponent } from './slu.component';


@NgModule({
  declarations: [SluComponent],
  imports: [
    CommonModule,
    SluRoutingModule
  ]
})
export class SluModule { }
