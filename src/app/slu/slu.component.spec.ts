import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SluComponent } from './slu.component';

describe('SluComponent', () => {
  let component: SluComponent;
  let fixture: ComponentFixture<SluComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SluComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SluComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
