import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DmComponent } from './dm.component';

const routes: Routes = [
	{
		path: '',
		component: DmComponent
	}
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DmRoutingModule { }
