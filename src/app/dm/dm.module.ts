import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DmRoutingModule } from './dm-routing.module';
import { DmComponent } from './dm.component';


@NgModule({
  declarations: [DmComponent],
  imports: [
    CommonModule,
    DmRoutingModule
  ]
})
export class DmModule { }
