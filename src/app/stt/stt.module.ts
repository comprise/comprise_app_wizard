import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SttRoutingModule } from './stt-routing.module';
import { SttComponent } from './stt.component';


@NgModule({
  declarations: [SttComponent],
  imports: [
    CommonModule,
    SttRoutingModule
  ]
})
export class SttModule { }
