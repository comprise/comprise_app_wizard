import { Component, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';

import { ProjectDataService, ElectronService, RestService, LoggingService } from '../core/services';

@Component({
  selector: 'app-stt',
  templateUrl: './stt.component.html',
  styleUrls: ['./stt.component.scss']
})
export class SttComponent implements AfterViewInit  {

  	constructor(private router: Router, 
		public projectDataService: ProjectDataService,
		public restService: RestService,
		public loggingService: LoggingService,
		public electronService: ElectronService) {
		console.log(this.router.url)
	}

	ngAfterViewInit(): void {}

}
