import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';

import { ProjectDataService } from './';

@Injectable({
  providedIn: 'root'
})
export class RestService {

	constructor(public projectDataService: ProjectDataService) {}
	
	 trainIntentModel(data) {
		return new Promise((resolve, reject) => {
			var xmlHttp = new XMLHttpRequest();

			xmlHttp.onreadystatechange = function() {
				if (xmlHttp.readyState === 4 && xmlHttp.status == 200) {
					console.log("" , xmlHttp)
					resolve(xmlHttp.responseText);
				} else if (xmlHttp.readyState === 4 && xmlHttp.status == 500) {
					reject(xmlHttp.responseText);
				}
			}	
			
			xmlHttp.open('POST', "https://dev-nlu1-am.azure-api.net/api/Train/"+this.projectDataService.cloudPlatfromAppID+"/"+this.projectDataService.cloudPlatfromLanguage+"/intents", true); // true for asynchronous
			xmlHttp.setRequestHeader("Ocp-Apim-Subscription-Key", this.projectDataService.ocp_apim_subscription_key);
			xmlHttp.setRequestHeader("Content-Type", "application/json-patch+json");
			xmlHttp.send(JSON.stringify(data));
		});
	}
	
	observeTrainingStatus() {
		return new Promise((resolve, reject) => {
			var xmlHttp = new XMLHttpRequest();
			xmlHttp.onreadystatechange = function() {
				if (xmlHttp.readyState === 4) {
					var response = xmlHttp.responseText; 
					response = response.replace(new RegExp("\\[", 'g'), "");
					response = response.replace(new RegExp("\\]", 'g'), "");
					response = response.replace(new RegExp("\"", 'g'), "");
					response = response.replace(new RegExp(",", 'g'), "<br>");
					resolve(response);
				}
			}	
			 
			xmlHttp.open('GET', "https://dev-nlu1-am.azure-api.net/api/Train/"+this.projectDataService.cloudPlatfromAppID+"/"+this.projectDataService.cloudPlatfromLanguage+"/intents/status", true); // true for asynchronous
			xmlHttp.setRequestHeader("Ocp-Apim-Subscription-Key", this.projectDataService.ocp_apim_subscription_key);
			xmlHttp.setRequestHeader("Content-Type", "application/json-patch+json");
			xmlHttp.send();
		});
	}
	
	checkTrainingQuality() {
		return new Promise((resolve, reject) => {
			var date = new Date();
			date.setHours(date.getHours() - 1);
			var time = date.toLocaleDateString("en-GB", {
			  hour12: true,
			  year: "2-digit",
			  month: "numeric",
			  day: "numeric",
			  hour: "numeric",
			  minute: "2-digit",
			  second: "2-digit"
			});
			time = time.replace(new RegExp(",", 'g'), "").toUpperCase();
			time += " | ";
			
			

			var xmlHttp = new XMLHttpRequest();
			xmlHttp.onreadystatechange = function() {
				if (xmlHttp.readyState === 4) {
					var result = "";
				 
					if(xmlHttp.responseText !== "[]") {
						result = time + "There are sentences with lower confidence on their own intents than 60%. Following models need improvement: <br>"
						JSON.parse(xmlHttp.responseText).forEach(item => {
							var parts = item.split("    ");
							result += time + "Sentence \""+parts[4]+"\" has a similar confidence for intent \""+parts[1]+"\" (~"+parseFloat(parts[0].substring(0,5)).toFixed(3)+") and intent \""+parts[3]+"\" (~"+parseFloat(parts[2].substring(0,5)).toFixed(3)+")<br>";
						});
						
						result += JSON.parse(xmlHttp.responseText);
						reject(result);
					} else {
						result += "Training data is in good shape, each sentence trained points to an intent with a higher confidence than 60%!";
						resolve(result);
					}
					
					
					
				}
			}	
			 
			xmlHttp.open('GET', "https://dev-nlu1-am.azure-api.net/api/Train/"+this.projectDataService.cloudPlatfromAppID+"/"+this.projectDataService.cloudPlatfromLanguage+"/intents/worst-confidence", true); // true for asynchronous
			xmlHttp.setRequestHeader("Ocp-Apim-Subscription-Key", this.projectDataService.ocp_apim_subscription_key);
			xmlHttp.setRequestHeader("Content-Type", "application/json-patch+json");
			xmlHttp.send();
		});
	}
	
	detectIntent(statement: string) {
		return new Promise((resolve, reject) => {
			var xmlHttp = new XMLHttpRequest();

			xmlHttp.onreadystatechange = function() {
				if (xmlHttp.readyState === 4) {
					var response = xmlHttp.responseText;
					response = "Insertion was: "+statement+"<br><br>"+ response ;
					response = response.replace(new RegExp("},", 'g'), "<br>");
					response = response.replace(new RegExp("\\[", 'g'), "");
					response = response.replace(new RegExp("\\]", 'g'), "");
					response = response.replace(new RegExp("{", 'g'), "");
					response = response.replace(new RegExp("}", 'g'), ""); 
					resolve(response);
				}
			}	 
			xmlHttp.open('POST', "https://dev-nlu1-am.azure-api.net/api/Guess/"+this.projectDataService.cloudPlatfromAppID+"/"+this.projectDataService.cloudPlatfromLanguage, true); // true for asynchronous
			xmlHttp.setRequestHeader("Ocp-Apim-Subscription-Key", this.projectDataService.ocp_apim_subscription_key);
			xmlHttp.setRequestHeader("Content-Type", "application/json-patch+json");
			xmlHttp.send("\""+statement+"\"");
		});
	}
}
