import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './shared/components';

const routes: Routes = [
  {
    path: '', redirectTo: 'home', pathMatch: 'full'
  },
  { path: 'home', loadChildren: () => import('./home/home.module').then(m => m.HomeModule) },
  { path: 'stt', loadChildren: () => import('./stt/stt.module').then(m => m.SttModule) },
  { path: 'slu', loadChildren: () => import('./slu/slu.module').then(m => m.SluModule) },
  { path: 'dm', loadChildren: () => import('./dm/dm.module').then(m => m.DmModule) },
  { path: 'cp', loadChildren: () => import('./cp/cp.module').then(m => m.CpModule) },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
